import { BrowserModule } from '@angular/platform-browser';
import { APP_INITIALIZER, Injectable, InjectionToken, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router'
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DestinoViajeComponent } from './components/destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './components/lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './components/destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './components/form-destino-viaje/form-destino-viaje.component';
import { ReactiveFormsModule } from '@angular/forms';
import { DestinosApiClient } from './models/destinos-api-client';
import {
  DestinosViajesActions, DestinosViajesState, DestinoViajesEffects,
  initializeDestinosViajesState, reducerDestinosViajes, InitDataAction
} from './models/destinos-viajes-state.model';
import { ActionReducerMap, Store, StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from "@ngrx/effects";
import { LoginComponent } from './components/login/login.component';
import { ProtectedComponent } from './components/protected/protected.component';
import { UsuarioLogueadoGuard } from './guards/usuario-logueado/usuario-logueado.guard';
import { AuthService } from './services/auth.service';
import { FormsModule } from '@angular/forms';
import { VuelosComponent } from './components/vuelos/vuelos/vuelos.component';
import { VuelosMainComponent } from './components/vuelos/vuelos-main/vuelos-main.component';
import { VuelosMasInfoComponent } from './components/vuelos/vuelos-mas-info/vuelos-mas-info.component';
import { VuelosDetalleComponent } from './components/vuelos/vuelos-detalle/vuelos-detalle.component';
import { ReservasModule } from './reservas/reservas.module';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Dexie } from 'dexie';
import { promise } from 'protractor';
import { DestinoViaje } from './models/destino-viaje.model';
import { TranslateLoader, TranslateService, TranslateModule } from '@ngx-translate/core';
import { from, Observable } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { NgxMapboxGLModule } from 'ngx-mapbox-gl';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';


export interface AppConfig {
  apiEndPoint: String;
}

export const APP_CONFIG_VALUE: AppConfig = {
  apiEndPoint: 'http://localhost:3000'
}

export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');

//redux init
export interface AppState {
  destinos: DestinosViajesState;
}

const reducers: ActionReducerMap<AppState, DestinosViajesActions> = {
  destinos: reducerDestinosViajes
};

const reducersInitialState = {
  destinos: initializeDestinosViajesState()
};

//redux fin init

export const rutasHijasVuelos: Routes = [
  { path: '', redirectTo: 'main', pathMatch: 'full' },
  { path: 'main', component: VuelosMainComponent },
  { path: 'mas-info', component: VuelosMasInfoComponent },
  { path: ':id', component: VuelosDetalleComponent },
];

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected', component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  },
  { path: 'vuelos', component: VuelosComponent, children: rutasHijasVuelos, canActivate: [UsuarioLogueadoGuard] }

]

//init app

@Injectable()
export class AppLoadService {
  constructor(private store: Store<AppState>, private http: HttpClient) { }
  async initializeDestinosViajesState(): Promise<any> {
    const headers: HttpHeaders = new HttpHeaders({ 'X-API-TOKEN': 'token-seguridad' });
    console.log(APP_CONFIG_VALUE + '/my');
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndPoint + '/my', { headers: headers });
    const response: any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitDataAction(response.body));

  }

}

export function init_app(appLoadService: AppLoadService): () => Promise<any> {
  return () => appLoadService.initializeDestinosViajesState();
}

export class Translation {
  constructor(public id: number, public lang: string, public key: string, public value: string) { }

}
//fin appinit

//dexie con i18n
@Injectable({ providedIn: 'root' })
export class MyDatabase extends Dexie {
  // private table: Dexie.Table<any> = null;
  public destinos!: Dexie.Table<DestinoViaje, number>;
  translations !: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    // destinos = new Dexie.Table<DestinoViaje, number>();
    this.version(1).stores({
      destinos: '++id, nombre, imageUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imageUrl',
      translations: '++id, lang, key, value',
    });
  }
}

export const db = new MyDatabase();
//fin dexie con i18n

//cargador de translations
export class TranslationLoader implements TranslateLoader {
  constructor(private http: HttpClient) { }

  getTranslation(lang: string): Observable<any> {
    console.log('bulk');
    const promise = db.translations.where('lang').equals(lang).toArray().then(
      results => {
        console.log(results.length);
        if (results.length === 0) {
          return this.http.get<Translation[]>(APP_CONFIG_VALUE.apiEndPoint + '/api/translation?lang=' + lang)
            .toPromise()
            .then(apiResults => {
              console.log('bulk ' + apiResults);
              db.translations.bulkAdd(apiResults);
              console.log("apiresults: " + apiResults);
              return apiResults;
            });
        }
        return results;
      }).then(traducciones => {
        console.log("traducciones cargadas");
        return traducciones;
      }).then(traducciones => {
        return traducciones.map(t => ({ [t.key]: t.value }));
      });
    return from(promise).pipe(flatMap(elems => from(elems)));
  }

}

function HttpLoaderFactory(http: HttpClient) {
  return new TranslationLoader(http);
}



//fin cargador de translations

@NgModule({
  declarations: [
    AppComponent,
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent,
    EspiameDirective,
    TrackearClickDirective

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule.forRoot(routes), //para registrar las rutas en el moduloW
    ReactiveFormsModule,
    FormsModule,
    NgRxStoreModule.forRoot(reducers,
      {
        initialState: reducersInitialState,
        runtimeChecks: {
          strictActionImmutability: false,
          strictStateImmutability: false
        }
      }),
    EffectsModule.forRoot([DestinoViajesEffects]),
    ReservasModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory),
        deps: [HttpClient]
      }
    }),
    NgxMapboxGLModule,
    BrowserAnimationsModule

  ],
  providers: [DestinosApiClient, AuthService,
    UsuarioLogueadoGuard, { provide: APP_CONFIG, useValue: APP_CONFIG_VALUE },
    AppLoadService,
    { provide: APP_INITIALIZER, useFactory: init_app, deps: [AppLoadService], multi: true },
    MyDatabase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
