import { Directive, ElementRef } from '@angular/core';
import { fromEvent } from 'rxjs';

@Directive({
  selector: '[appTrackearClick]'
})
export class TrackearClickDirective {

  private element!: HTMLInputElement;

  constructor(private elRef: ElementRef) { 
    console.log("creando traker");
    this.element = elRef.nativeElement;
    fromEvent(this.element, 'click').subscribe(evento => this.track(evento));
  }

  track(evento: Event):void{
    console.log("a trakear");
    const elemTags = this.element.attributes.getNamedItem('data-trackear-tags')?.value.split(' ');
    console.log(`||||||||||| track evento "${elemTags}"`);
  }

}
