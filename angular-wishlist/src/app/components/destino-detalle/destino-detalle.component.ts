import { HttpClient } from '@angular/common/http';
import { Component, Inject, Injectable, InjectionToken, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { Style } from 'mapbox-gl';
import { AppConfig, AppState, APP_CONFIG } from 'src/app/app.module';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';
import { DestinosApiClient } from "../../models/destinos-api-client";


@Injectable()
export class DestinosApiClientDecorated extends DestinosApiClient {
  constructor(store: Store<AppState>, @Inject(APP_CONFIG) config: AppConfig, http: HttpClient) {
    super(store, config, http);
  }
  getById(id: string): DestinoViaje {
    console.log('llamando por la clase decorada!');
    console.log('config: ' + this.config.apiEndPoint);
    return super.getById(id);
  }
}

class DestinosApiClientViejo {
  getById(id: string): DestinoViaje | any {
    console.log('llamado por la clase vieja!!');
    return null;
  }
}


@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }], 
})
export class DestinoDetalleComponent implements OnInit {
  destino!: DestinoViaje;
  public style: Style = {
    sources: {
      world: {
        type: 'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers: [{
      id: 'countries',
      type: 'fill',
      source: 'world',
      layout: {},
      paint: {
        'fill-color': '#6F788A',
      }
      
    }]
  };

  constructor(private route: ActivatedRoute, private destinosApiClient: DestinosApiClient) {
    //  this.destino = new DestinoViaje('','');
  }

  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id as string);
    console.log(this.destino);
  }

}
