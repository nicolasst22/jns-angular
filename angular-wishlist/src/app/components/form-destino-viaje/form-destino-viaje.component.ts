import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { DestinoViaje } from '../../models/destino-viaje.model';
import {map, filter, debounceTime, distinctUntilChanged, switchMap, flatMap} from 'rxjs/operators';
import { Observable, fromEvent } from 'rxjs';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import {APP_CONFIG, APP_CONFIG_VALUE, AppConfig} from 'src/app/app.module';

@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {

  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  public fg: FormGroup;
  minLongitud = 3;
  searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) {
    this.onItemAdded = new EventEmitter();
    this.searchResults = [];
    this.fg = fb.group({
      nombre: fb.control('', [Validators.required, this.nombreValidator, this.nombreValidatorParametrizable(this.minLongitud)]),
      url: fb.control(''/*valor*/, [Validators.required]/*validadores*/)
    });
    //console.log(this.fg.controls['nombre']);
    this.fg.valueChanges.subscribe((form:any) =>{
      console.log("cambio en el formulario: ", form);
    });

  }

  nombreValidator(control: FormControl): { [s: string]: boolean } {
    let l = control.value.toString().length;
    if (l > 0 && l < 5) {
      return { invalidNombre: true };
    }
    return {};
  }


  nombreValidatorParametrizable(minLong: number): ValidatorFn {

    return (control: AbstractControl): { [s: string]: boolean } | null => {

      let l = control.value.toString().length;
      if (l > 0 && l < this.minLongitud) {
        return { minLongNombre: true };
      }
      return {};

    }
  }

  ngOnInit(): void {

    let elementNombre = <HTMLInputElement>document.getElementById("nombre");
    fromEvent(elementNombre, 'input' )
    .pipe(
      map( e => (e.target as HTMLInputElement).value),
      filter((text: string) => text.length >= 3),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndPoint+"/ciudades?q="+text))
         //api de busqueda


    ).subscribe(ajaxResponse => {
      console.log(ajaxResponse); 
      if(ajaxResponse){ // cuando no concuerda devuelvo null asi que pregunto para no incluir en las opciones
     
       this.searchResults = ajaxResponse.response;
        
      }
    });
  }

  filter(val:string){
   
    let results: string[] = [];
    return ajax('/assets/datos.json').pipe(
    flatMap( (res: AjaxResponse) => res.response),
    map(e => {
         if((e as string).toUpperCase().startsWith(val.toUpperCase())){
            results.push((e as string));
            return e;
           }else{
             return null;
           }
    }), 
    
   );
  
  }

  guardar(nombre: string, url: string): boolean {
    if (this.fg.valid) {
      let d = new DestinoViaje(nombre, url);
      this.onItemAdded.emit(d);

    } else {
      this.fg.markAllAsTouched();
    }


    return false;
  }

}
