import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  mensajeError: string;
  

  constructor(public authService: AuthService) {
    this.mensajeError = "";

  }

  ngOnInit(): void {
  }

  login(usuario: string, pass: string): boolean {
    this.mensajeError = "";
    if (!this.authService.login(usuario, pass)) {
      this.mensajeError = "Usuario o contraseña inválida";
      setTimeout(()=> {
        this.mensajeError = "";
      }, 2500);
    }
    return false;
  }

  logout(): boolean {
    this.authService.logout();
    return false;
  }

}
