import { Component, OnInit, Input, HostBinding, EventEmitter, Output } from '@angular/core';
import { NumberValueAccessor } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { VoteUpAction, VoteDownAction, EliminarDestinoAction } from '../../models/destinos-viajes-state.model';
import {trigger, state, style, transition, animate} from '@angular/animations';

@Component({
  selector: 'app-destino-viaje',
  templateUrl: './destino-viaje.component.html',
  styleUrls: ['./destino-viaje.component.css'],
  animations: [
      trigger("esFavorito", [
        state('estadoFavorito', style({
          backgroundColor: 'PaleTurquoise'
        })),
        state('estadoNoFavorito', style({
          backgroundColor: 'WhiteSmoke'
        })),
        transition('estadoNoFavorito => estadoFavorito', [animate('3s')]),
        transition('estadoNoFavorito => estadoFavorito', [animate('1s')]),

      ])
  ]
 
})
export class DestinoViajeComponent implements OnInit {

  @Input() destino: DestinoViaje;
  @Input("idx") position: number;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  @Output() onClicked: EventEmitter<DestinoViaje>;

  constructor(private store: Store<AppState>) {
    this.destino = new DestinoViaje('Sin', "Asignar");
    this.onClicked = new EventEmitter();
    this.position = 0;
   
   }

  ngOnInit(): void {
  }

  ir():boolean {
    console.log("ir "+this.destino);
    this.onClicked.emit(this.destino);
    return false;
  }

  voteUp(){
    console.log("up");
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }

  voteDown(){
    console.log("down");
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }

 remove(){
    console.log("remove");
    this.store.dispatch(new EliminarDestinoAction(this.destino));
    return false;
  }


}
