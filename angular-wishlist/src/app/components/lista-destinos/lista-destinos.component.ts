import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { fromEvent } from 'rxjs';
import { AppState } from '../../app.module';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { DestinosApiClient } from '../../models/destinos-api-client';
import { NuevoDestinoAction, DestinoFavoritoAction } from '../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css']
})
export class ListaDestinosComponent implements OnInit {

  destinos: DestinoViaje[];
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  public all: DestinoViaje[];


  constructor(public destinosApiClient: DestinosApiClient, private store: Store<AppState>) {
    this.destinos = [];
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito).subscribe(d => {
      //const fav = data;
      if (d != null) {
        this.updates.push("Se ha elegido a " + d.nombre);
        console.log("funcion sbs");
      }

    });
    this.all = [];
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
    // this.destinosApiClient.subscrineOnChange((d: DestinoViaje)=>{

    // });


  }

  ngOnInit(): void {


  }

  agregado(d: DestinoViaje) {
    this.destinosApiClient.add(d);
    this.onItemAdded.emit(d);
    console.log('agregado');
  }

  elegido(d: DestinoViaje) {
    this.destinosApiClient.elegir(d);
    console.log('elegido');
  }

  getAll(): any {
    this.store.select(state => state.destinos.items);
  }



}
