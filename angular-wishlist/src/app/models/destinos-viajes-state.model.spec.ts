

import{ reducerDestinosViajes, DestinosViajesState, initializeDestinosViajesState, 
    NuevoDestinoAction,
    InitDataAction} from './destinos-viajes-state.model';


import {DestinoViaje} from './destino-viaje.model';

describe("reducerDestinoViajes", ()=>{
    //setup
    it('should reduce init data', ()=>{
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitDataAction = new InitDataAction(['destino 1', 'destino 2']);
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        //assertions / reglas de control
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].nombre).toEqual("destino 1");
        // teardown
        // deshacer
        
    });

    it('should reduce new itema added', ()=>{
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('Barcelona','url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].nombre).toEqual("Barcelona");
    });



});