import { Action } from '@ngrx/store';
import {Actions, Effect, ofType} from '@ngrx/effects';
import { DestinoViaje } from "./destino-viaje.model";
import { Injectable } from '@angular/core';
import { map} from 'rxjs/operators';
import {Observable, of} from 'rxjs' 
import { AppState } from '../app.module';
import {HttpClientModule} from '@angular/common/http';


export interface DestinosViajesState {
    items: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje | null;
}


export const initializeDestinosViajesState = function(){
    return {items: [],
            loading: false,
            favorito: null
    }
}

export enum DestinosViajesActionTypes {
    NUEVO = "[Destinos Viajes ] Nueva",
    FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP ='[Destinos Viajes] Vote UP',
    VOTE_DOWN ='[Destinos Viajes] Vote DOWN',
    REMOVE = '[Destinos Viajes] Eliminar',
    INIT_MY_DATA = '[Destinos Viajes] Inicializando'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionTypes.NUEVO;
    constructor(public destino: DestinoViaje){};
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionTypes.VOTE_UP;
    constructor(public destino: DestinoViaje){};
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionTypes.VOTE_DOWN;
    constructor(public destino: DestinoViaje){};
}


export class DestinoFavoritoAction implements Action {
    type = DestinosViajesActionTypes.FAVORITO;
    constructor(public destino: DestinoViaje){};
}


export class EliminarDestinoAction implements Action {
    type = DestinosViajesActionTypes.REMOVE;
    constructor(public destino: DestinoViaje){};
}

export class InitDataAction implements Action {
    type = DestinosViajesActionTypes.INIT_MY_DATA;
    constructor(public destinos: string[]){};
}

export type DestinosViajesActions = NuevoDestinoAction | DestinoFavoritoAction
|VoteUpAction |VoteDownAction| EliminarDestinoAction| InitDataAction;

//Reducers: son llamados en el orden que aparecen en el codigo
export function reducerDestinosViajes(
    state: DestinosViajesState = initializeDestinosViajesState(),
    action: Action
  ): DestinosViajesState {
    switch(action.type){
        case DestinosViajesActionTypes.NUEVO: {
            return {
                ...state, //preserva el resto del state. Abajo muta el estado
                items: [...state.items, (action as NuevoDestinoAction).destino]

            };
        }
        case DestinosViajesActionTypes.REMOVE: {
           var index = state.items.indexOf((action as EliminarDestinoAction).destino);
           state.items.splice(index, 1);
            return {
                ...state, //preserva el resto del state. Abajo muta el estado
                items: [...state.items]
            };
        }
        case DestinosViajesActionTypes.FAVORITO: {
            (state as DestinosViajesState).items.forEach(x =>{
                x.setSelected(false);
            });
            const fav: DestinoViaje = (action as DestinoFavoritoAction).destino;
            fav.setSelected(true);
            return {
                ...state,
                favorito: fav

            };
        }
        case DestinosViajesActionTypes.VOTE_UP: {
            const d: DestinoViaje = (action as VoteUpAction).destino; 
            d.voteUp();
            return {
                ...state
            };
        }

        case DestinosViajesActionTypes.VOTE_DOWN: {
            const d: DestinoViaje = (action as VoteDownAction).destino; 
            d.voteDown();
            return {
                ...state
            };
        }

        case DestinosViajesActionTypes.INIT_MY_DATA: {
            const destinos: string[] = (action as InitDataAction).destinos;
            return {
                ...state, 
                items: destinos.map((d) => new DestinoViaje(d, ''))

            };
        }

    }

    return state;
}

//effects
@Injectable()
export class DestinoViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
       ofType(DestinosViajesActionTypes.NUEVO), 
       map((action: NuevoDestinoAction) => new DestinoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions){}

}