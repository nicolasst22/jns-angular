1. Debe verse un Guard que verifique, usando un servicio, si un usuario está logueado o no.
en /guard/usuario-logueado
export class UsuarioLogueadoGuard implements CanActivate {

  constructor(private authService: AuthService){

  }
 
  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
     const isLoggedIn = this.authService.isLoggedIn();
     console.log("canactivate", isLoggedIn);
    return isLoggedIn;
  }
  
}


2. Debe verse un componente que sea protegido por un Guard cuando se configura su ruta.
en app.module.ts
const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: ListaDestinosComponent },
  { path: 'destino/:id', component: DestinoDetalleComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'protected', component: ProtectedComponent,
    canActivate: [UsuarioLogueadoGuard]
  },
  { path: 'vuelos', component: VuelosComponent, children: rutasHijasVuelos, canActivate: [UsuarioLogueadoGuard] }

]

3. Debe verse una variable de configuración u otro literal, que es inyectado como dependencia con un InjectionToken propio.
/models/DestinosApiClient 
se inyecta la configuracion APP_CONFIG mediante @Inject y se salva referencia ciclica con forwrdRef

export class DestinosApiClient {
	destinos: DestinoViaje[] = [];
	constructor(private store: Store<AppState>, 
		@Inject(forwardRef(()=>APP_CONFIG)) private config: AppConfig,
		private http: HttpClient
		) {
		this.store.select(state => state.destinos).subscribe((data)=> {
			console.log('destinos sub store');
			console.log(data);
			this.destinos = data.items;
		});
		this.store.subscribe((data) => {
			console.log('all store');
			console.log(data);			
		});
	}

4. Debe verse una inyección de dependencias configurada con un useClass para vincular, por ejemplo, una interfaz a una clase concreta.
/components/desti-detalle/destino-detalle.component.ts

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }], 
})


5. Debe verse una inyección de dependencias con un useExisting, que inyecte una clase X con una clase Y, que debe ser compatible, por ejemplo, porque Y hereda de X o, por ejemplo, porque X e Y heredan de Z.
/components/desti-detalle/destino-detalle.component.ts

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [
    { provide: DestinosApiClient, useClass: DestinosApiClientDecorated },
    { provide: DestinosApiClientViejo, useExisting: DestinosApiClient }], 
})


6. Debe existir una aplicación express, que oficie de una API simple, que maneje un array en memoria de elementos y que debe retornar el array de elemento con un get y también debe permitir agregar elementos recibiendo JSON en un post.
  en el repositorio en la carpeta llamada "express-server"


7. Debe verse un servicio en angular que se comunique asíncronamente con el api usando Http.
 en DestinosApiClient 

	add(d: DestinoViaje) {
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token de seguridad', observe: 'response'});
		const req = new HttpRequest('POST', this.config.apiEndPoint+"/my", {
			nuevo: d.nombre
		}, {headers: headers});
		console.log(this.config.apiEndPoint+"/my");
		const body = {
			nuevo: d.nombre
		};
		console.log(body);
			this.http.request(req).subscribe(data => {   //ESTA LINE HACE LA PETICION REST (GET) AL SERVER EXPRESS
 			if((data as HttpResponse<{}>).status === 200){
				this.store.dispatch(new NuevoDestinoAction(d));
				const myDb = db;
				myDb.destinos.add(d);
				console.log('todos los detinos de la db');
				myDb.destinos.toArray().then((destino: any) => console.log(destino));
			} 
		  });
	}

8. Debe verse que el servicio que se comunica con el API, cuando asíncronamente el API retorne una respuesta afirmativa ante el agregado de un elemento, el servicio creado debe notificar a Redux con un Action. (Recordar, ese Action debe ser usado al menos para que se agregue ese nuevo elemento al estado de Redux, y por ende, repercuta en la interfaz de usuario).

en DestinosApiClient 

	add(d: DestinoViaje) {
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token de seguridad', observe: 'response'});
		const req = new HttpRequest('POST', this.config.apiEndPoint+"/my", {
			nuevo: d.nombre
		}, {headers: headers});
		console.log(this.config.apiEndPoint+"/my");
		const body = {
			nuevo: d.nombre
		};
		console.log(body);
			this.http.request(req).subscribe(data => {  
			if((data as HttpResponse<{}>).status === 200){  //CONTROLO QUE EL STATUS SEA VALIDO (RTA AFIRMATIVA)
				this.store.dispatch(new NuevoDestinoAction(d));
				const myDb = db;
				myDb.destinos.add(d);
				console.log('todos los detinos de la db');
				myDb.destinos.toArray().then((destino: any) => console.log(destino));
			} 
		  });
	}

9. Debe verse una base de datos Dexie con al menos una entidad y que sea inyectada a un componente o servicio.
EN app.module.ts

@Injectable({ providedIn: 'root' })
export class MyDatabase extends Dexie {
  // private table: Dexie.Table<any> = null;
  public destinos!: Dexie.Table<DestinoViaje, number>;
  translations !: Dexie.Table<Translation, number>;
  constructor() {
    super('MyDatabase');
    // destinos = new Dexie.Table<DestinoViaje, number>();
    this.version(1).stores({
      destinos: '++id, nombre, imageUrl',
    });
    this.version(2).stores({
      destinos: '++id, nombre, imageUrl',
      translations: '++id, lang, key, value',
    });
  }
}


10. Debe verse que el servicio que se comunica con el API, cuando se agrega un elemento de manera exitosa en el api, debe agregarse asíncronamente también en Dexie.

	add(d: DestinoViaje) {
		const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token de seguridad', observe: 'response'});
		const req = new HttpRequest('POST', this.config.apiEndPoint+"/my", {
			nuevo: d.nombre
		}, {headers: headers});
		console.log(this.config.apiEndPoint+"/my");
		const body = {
			nuevo: d.nombre
		};
		console.log(body);
			this.http.request(req).subscribe(data => {   
 			if((data as HttpResponse<{}>).status === 200){ //SI ES EXITOSO
				this.store.dispatch(new NuevoDestinoAction(d));
				const myDb = db;
				myDb.destinos.add(d);                         //ADD A DEXIE
				console.log('todos los detinos de la db');
				myDb.destinos.toArray().then((destino: any) => console.log(destino));
			} 
		  });
	}
