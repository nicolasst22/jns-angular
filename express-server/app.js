// var express = require('express');
// var app = express();
// app.use(express.json);
// var port = 3000;

const express = require('express'), cors = require('cors');
const fs = require('fs');
const app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.json()); // body en formato json
app.use(bodyParser.urlencoded({ extended: false }));
const port = 3000;
app.use(cors());

var misDestinos = [];
app.get('/', (req, res) => {
    console.log("/");
    res.end('Hello World!');
});
app.get("/url", (req, res, next) => {
    console.log("/url");
    res.json(["Paris", "Barcelona", "Barranquilla", "Montevideo", "Santiago de Chile",
        "Mexico DF", "Nueva York"]);
}
);

app.get("/ciudades", (req, res, next) => {
    console.log("/ciudades");
    //var ciudades = ajax('/assets/datos.json')
    let rawdata = fs.readFileSync('datos.json');
    let ciudades = JSON.parse(rawdata);
    console.log(ciudades);
    console.log("q "+req.query.q.toString().toLowerCase());
    var filtradas = ciudades.filter(
        (c)=>c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1);
    res.json(filtradas);     
}
);

app.get("/api/translation", (req, res, next) => {
    console.log("getting translation "+ req.query.lang);
    res.json([{lang: req.query.lang, key: 'Hola', value: 'Hola ' + req.query.lang}])
}
);


app.get("/my", (req, res, next) => res.json(misDestinos));
app.post("/my", (req, res, next) => {
    console.log(req.body);
  //  misDestinos = req.body;
    misDestinos.push(req.body.nuevo);
    res.json(misDestinos);
});

app.listen(port, ()=> {console.log("Server en "+port)});
console.log('App Cargado');



// app.get('/', (req, res) => {
//   res.send('Hello World!')
// })

// app.listen(port, () => {
//   console.log(`Example app listening at http://localhost:${port}`)
// })